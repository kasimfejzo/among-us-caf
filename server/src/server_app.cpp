#include "caf/actor_control_block.hpp"
#include "caf/actor_system_config.hpp"
#include "caf/event_based_actor.hpp"
#include "caf/send.hpp"
#include "caf/stateful_actor.hpp"
#include <caf/all.hpp>
#include <string>
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>
#include <nlohmann/json.hpp>
#include <sys/ioctl.h>
#include <future>

#define PORT 8080

struct AddNewClient {
  int socket;
  std::string name;
};

template <class Inspector>
typename Inspector::result_type inspect(Inspector& f, AddNewClient& x) {
  return f(caf::meta::type_name("AddNewClient"), x.socket, x.name);
}

struct GetData {
  int socket;
  std::string name;
  std::string color;
  int room;
};

template <class Inspector>
typename Inspector::result_type inspect(Inspector& f, GetData& x) {
  return f(caf::meta::type_name("GetData"), x.socket, x.name, x.color, x.room);
}

struct CreateRoom {
  int socket;
  std::string message;
  uint index;
};

template <class Inspector>
typename Inspector::result_type inspect(Inspector& f, CreateRoom& x) {
  return f(caf::meta::type_name("CreateRoom"), x.socket, x.message, x.index);
}


struct JoinRoom {
  int socket;
  std::string message;
  uint index;
};

template <class Inspector>
typename Inspector::result_type inspect(Inspector& f, JoinRoom& x) {
  return f(caf::meta::type_name("JoinRoom"), x.socket, x.message, x.index);
}

struct LeaveRoom {
  int socket;
  std::string message;
  uint index;
};

template <class Inspector>
typename Inspector::result_type inspect(Inspector& f, LeaveRoom& x) {
  return f(caf::meta::type_name("LeaveRoom"), x.socket, x.message, x.index);
}

struct SetRoom {
  int roomId;
};

template <class Inspector>
typename Inspector::result_type inspect(Inspector& f, SetRoom& x) {
  return f(caf::meta::type_name("SetRoom"), x.roomId);
}

struct SetColor {
  std::string color;
};

template <class Inspector>
typename Inspector::result_type inspect(Inspector& f, SetColor& x) {
  return f(caf::meta::type_name("SetColor"), x.color);
}

struct UpdateEveryoneInRoomJoin {
  int roomId;
  std::string name;
  std::string color;
};

template <class Inspector>
typename Inspector::result_type inspect(Inspector& f, UpdateEveryoneInRoomJoin& x) {
  return f(caf::meta::type_name("UpdateEveryoneInRoomJoin"), x.roomId, x.name, x.color);
}

struct UpdateEveryoneInRoomLeft {
  int roomId;
  std::string name;
  std::string color;
};

template <class Inspector>
typename Inspector::result_type inspect(Inspector& f, UpdateEveryoneInRoomLeft& x) {
  return f(caf::meta::type_name("UpdateEveryoneInRoomLeft"), x.roomId, x.name, x.color);
}


struct ClientActorState {
  int socket;
  std::string name;
  std::string color = "";
  int room = 0;
};

caf::behavior clientActor(caf::stateful_actor<ClientActorState>* self, int socket, std::string name) {
  self -> state.socket = socket;
  self -> state.name = name;

  return caf::behavior {
    [self] (GetData) {
      return GetData{self -> state.socket, self -> state.name, self -> state.color, self -> state.room};
    },

    [self] (SetRoom event) {
      self -> state.room = event.roomId;
    },

    [self] (SetColor event) {
      self -> state.color = event.color;
    }
  };
}

struct ServerActorState {
  std::vector<caf::actor> actors;
  std::map<int, std::vector<std::string>> roomColors; //int represents roomId, and vector of strings the actual possibilities for colors --> 12550 -> RED, BLUE, WHITE, ORANGE
};

caf::behavior serverActor(caf::stateful_actor<ServerActorState>* self) {
  return caf::behavior {
    [self] (AddNewClient event) {
      self -> state.actors.push_back(self -> home_system().spawn(clientActor, event.socket, event.name));
      aout(self) << "SERVER: " + event.name + " joined!" << std::endl;
    },


    [self](CreateRoom event) {
      int roomId = rand() % 10000 + 10000;
      self -> send(self -> state.actors[event.index], SetRoom{roomId});
      self -> state.roomColors[roomId] = {std::string("BLUE"), std::string("GREEN"), std::string("RED"), std::string("ORANGE"), std::string("PURPLE"), std::string("WHITE"), std::string("NAVY"), std::string("PINK"), std::string("CORAL"), std::string("GREY")};

      send(event.socket, (std::string("Created room with id: " + std::to_string(roomId))).c_str(), 27, 0);
    },

    [self](JoinRoom event) {
      std::cout << "A" << std::endl;
      nlohmann::json msg = nlohmann::json::parse(event.message);

      std::cout << "B" << std::endl;
      self -> send(self -> state.actors[event.index], SetRoom{msg["room"]});
      self -> send(self -> state.actors[event.index], SetColor{self -> state.roomColors[msg["room"]].back()});
      self -> state.roomColors[msg["room"]].pop_back();
     
      std::cout << "C" << std::endl;

      int room = msg["room"];
      send(event.socket, std::string("Players in room: ").c_str(), 17, 0);
      for(uint i = 0; i < self -> state.actors.size(); ++i) {
        self -> request(self -> state.actors[i], caf::infinite, GetData{}).await([=](GetData data) {
          if(room == data.room) {
            std::cout << "ABOVE ITERATION 3..." << std::endl;
            std::cout << "ABOVE ITERATION 2..." << std::endl;
            std::cout << "ABOVE ITERATION 1..." << std::endl;
            send(event.socket, data.name.c_str(), data.name.size(), 0);
          }
          std::cout << "ITERATION" << std::endl;
        }); 
      }

      std::cout << "D" << std::endl;

      self -> request(self -> state.actors[event.index], caf::infinite, GetData{}).await([self](GetData event) { 
        self -> send(self, UpdateEveryoneInRoomJoin{event.room, event.name, event.color});
      });
    },

    [self](LeaveRoom event) {
      int room = -1;
      std::string color = "-1";
      self -> request(self -> state.actors[event.index], caf::infinite, GetData{}).await([self, room, color](GetData event) mutable {
        room = event.room;
        color = event.color;
        self -> send(self, UpdateEveryoneInRoomLeft{event.room, event.name, event.color});
      });

      self -> send(self -> state.actors[event.index], SetRoom{0});
      self -> send(self -> state.actors[event.index], SetColor{""});
      self -> state.roomColors[room].push_back(color);
    },

    [self](UpdateEveryoneInRoomJoin event) {
      for(uint i = 0; i < self -> state.actors.size(); ++i) {
        self -> request(self -> state.actors[i], caf::infinite, GetData{}).await([event](GetData data) {
          std::string message = event.name + " joined with color " + event.color;
          if(event.roomId == data.room && event.name != data.name) send(data.socket, message.c_str(), message.size(), 0);
        }); 
      }
    },

    [self](UpdateEveryoneInRoomLeft event) {
      for(uint i = 0; i < self -> state.actors.size(); ++i) {
        self -> request(self -> state.actors[i], caf::infinite, GetData{}).await([event](GetData data) {
          std::string message = event.name + " left";
          if(event.roomId == data.room && event.name != data.name) send(data.socket, message.c_str(), message.size(), 0);
        }); 
      }
    }
  };
}

void checkForMessages(std::vector<int>&, caf::actor);

int main() {
  int server_fd;
  struct sockaddr_in address;
  int addr_len = sizeof(address);

  server_fd = socket(AF_INET, SOCK_STREAM, 0);

  address.sin_family = AF_INET;
  address.sin_addr.s_addr = INADDR_ANY;
  address.sin_port = htons(PORT);

  bind(server_fd, (sockaddr*) &address, sizeof(address));

  caf::actor_system_config cfg;
  caf::actor_system sys{cfg};

  caf::actor serverAct = sys.spawn(serverActor);

  std::vector<int> actorSockets;

  auto fut = std::async(std::launch::async, checkForMessages, std::ref(actorSockets), serverAct);

  while(true) {
    if(listen(server_fd, SOMAXCONN)) continue;

    int new_fd;
    if((new_fd = accept(server_fd, (sockaddr*) &address, (socklen_t*) &(addr_len))) < 0) continue;
  
    char buffer[1024] = {0};
    read(new_fd, buffer, 1024);
    caf::anon_send(serverAct, AddNewClient{new_fd, std::string(buffer)});
    std::cout << "PUSH" << std::endl;
    actorSockets.push_back(new_fd);
    std::cout << "NAKON" << std::endl;
    
    int iMode = 1;
    ioctl(new_fd, FIONBIO, &iMode);
  }

  return 0;
}

void checkForMessages(std::vector<int>& actorSockets, caf::actor serverAct) {
  while(1) {
    for(uint i = 0; i < actorSockets.size(); ++i) {
      char buffer[1024] = {0};
      //std::cout << "A" << std::endl;
      read(actorSockets[i], buffer, 1024); 
      //std::cout << "B" << std::endl;
      
      std::string msg(buffer);
      if(msg.size() > 0) {
        std::cout << "A" << std::endl;
        std::cout << msg.size() << msg << std::endl;
        std::cout << "B" << std::endl;
      }
      if(msg.size() > 0) {
        nlohmann::json jMsg = nlohmann::json::parse(msg);
        
        if(jMsg["to"] == "SERVER" && jMsg["message"] == "CREATE ROOM") caf::anon_send(serverAct, CreateRoom{actorSockets[i], msg, i});
        else if(jMsg["to"] == "SERVER" && jMsg["message"] == "JOIN") caf::anon_send(serverAct, JoinRoom{actorSockets[i], msg, i});
        else if(jMsg["to"] == "SERVER" && jMsg["message"] == "LEAVE") caf::anon_send(serverAct, LeaveRoom{actorSockets[i], msg, i});
      }
    }
  }
}
