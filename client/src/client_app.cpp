#include <stdio.h> 
#include <sys/socket.h> 
#include <arpa/inet.h> 
#include <unistd.h> 
#include <string.h> 
#include <caf/all.hpp>
#include <future>
#include <nlohmann/json.hpp>

#define PORT 8080  

void readInput(const int&);

int main(int argc, char const *argv[]) { 
  int sock = 0; 
  struct sockaddr_in serv_addr;
  
  if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) { 
    return -1; 
  } 
   
  serv_addr.sin_family = AF_INET; 
  serv_addr.sin_port = htons(PORT); 
  
  if(inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr) <= 0) { 
    return -1; 
  } 
   
  if (connect(sock, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0) { 
    return -1; 
  }

  //just first time for name
  
  std::string s;
  std::getline(std::cin, s);

  send(sock, s.c_str(), s.size(), 0);
  std::cout << s << std::endl;

  //end
  
  auto fut = std::async(std::launch::async, readInput, sock);
    
  while(1) {
    char buffer[1024] = {0}; 
    read(sock , buffer, 1024);
    printf("%s\n", buffer);
  }
 
  std::cout << "" <<std::endl;
  return 0; 
}

void readInput(const int& sock) {
  while(1) {
    nlohmann::json j;
    
    std::string to;
    std::getline(std::cin, to);

    std::string message;
    std::getline(std::cin, message);

    if(to == "SERVER" && message == "JOIN") {
      int room;
      std::cin >> room;
      j["room"] = room;
    }
    
    j["to"] = to;
    j["message"] = message;
    std::string s(j.dump());

    send(sock, s.c_str(), s.size(), 0);
    std::cout << s << std::endl;
  }
}

